import React, { useState } from "react";


export default function AddPerson() {
    const [person, setPerson] = useState({
        name:'',
        firstname:'',
        age:0
    });

    const addPerson = () => {
        fetch('http://localhost:4000/person', {
            method:'POST',
            body: person
        }).then(response => response.json())
        .then(data => console.log(data));
    }

    return (
        <section>
            <h1>Add Person</h1>

            <form>
                <div className="form-group">
                    <label htmlFor="firstname">Firstname</label>
                    <input type="text" className="form-control" id="firstname" value={person.firstname} />

                </div>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" id="name" value={person.name} />
                </div>
                <div className="form-group">
                    <label htmlFor="age">Age</label>
                    <input type="number" className="form-control" id="age" value={person.age} />
                </div>
                <button onClick={addPerson} type="submit" className="btn btn-primary">Submit</button>
            </form>

        </section>
    )
}