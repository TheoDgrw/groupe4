import React from 'react';

export default function ModalPerson({selected,close}){
    return(
        <div className="modal" tabIndex="-1" role="dialog" style={{display:'block'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{selected.name}</h5>
                        <button type="button" className="close" onClick={() => close()} >
                            <span>&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>Firstname : {selected.firstname}</p>
                        {selected.age === -1 ? <p>Deceased</p> : <p>Age : {selected.age}</p>}
                        
                    </div>
                    </div>
                </div>
            </div>
    );
}