import React, { useEffect, useState } from "react";
import { useListPersons} from '../Hooks/getPersons';
import ListPersons from './ListPersons';
import ModalPerson from './ModalPerson';


export default function Home() {

    const persons = useListPersons()
    const [selected, setSelected] = useState(null);

    return (
        <section>
            <ListPersons users={persons} selected={selected} updateSelected={(person) => setSelected(person)} />
            
            {selected && 
                <ModalPerson selected={selected} close={() => setSelected(null)} />
            }
        </section>
    )
}